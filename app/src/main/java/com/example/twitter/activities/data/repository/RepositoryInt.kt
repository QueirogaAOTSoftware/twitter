package com.example.twitter.activities.data.repository


import com.example.twitter.activities.Tweet
import com.example.twitter.activities.TweetListData
import com.example.twitter.activities.TwitterProfileData
import dagger.Provides
import io.reactivex.Observable

interface RepositoryInt {


    fun search(query: String): Observable<TweetListData>
    fun searchtimeline(query: String): Observable<List<Tweet>>
    fun searchprofile(query: String): Observable<TwitterProfileData>
}