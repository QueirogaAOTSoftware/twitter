package com.example.twitter.activities

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.twitter.R

class TweetListAdapter(internal var context: Context, private val listener:OnItemClickListener) :
    RecyclerView.Adapter<TweetListAdapter.ViewHolder>() {

    var tweets: List<Tweet> = mutableListOf()
        set(value) {
            field=value
            notifyDataSetChanged()
        }

    fun gettweet(position: Int): Tweet {
        return tweets[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.tweet_list_layout, parent, false)



        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val (user, body) = tweets[position]

        holder.title.text = user!!.userName
        holder.message.text = body
        Glide.with(context)
            .load(user.profilepic)
            .centerCrop()
            .into(holder.imageView)

    }


    override fun getItemCount(): Int {
        return tweets.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
    View.OnClickListener{

        val title: TextView = itemView.findViewById<View>(R.id.title) as TextView
        val message: TextView = itemView.findViewById<View>(R.id.message) as TextView
        val imageView: ImageView = itemView.findViewById<View>(R.id.userImage) as ImageView

        init {
            itemView.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            val position: Int = adapterPosition
            if(position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(position: Int)
    }
}
