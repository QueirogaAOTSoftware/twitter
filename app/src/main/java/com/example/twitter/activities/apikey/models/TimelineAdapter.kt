package com.example.twitter.activities

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.twitter.R
import java.util.ArrayList

class TimelineAdapter(internal var context: Context) :
        RecyclerView.Adapter<TimelineAdapter.ViewHolder>() {
    var tweetsList : List<Tweet> = mutableListOf()
        set(value) {
            field=value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.tweet_list_layout, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val (user, body) = tweetsList[position]
        holder.title.text = user?.userName
        holder.message.text = body
        Glide.with(context)
                .load(user?.profilepic)
                .centerCrop()
                .into(holder.imageView)


    }

    override fun getItemCount(): Int {
        return tweetsList.size
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById<View>(R.id.title) as TextView
        val message: TextView = itemView.findViewById<View>(R.id.message) as TextView
        val imageView: ImageView = itemView.findViewById<View>(R.id.userImage) as ImageView
    }
}
