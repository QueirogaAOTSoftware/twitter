package com.example.twitter.activities.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.twitter.activities.activities.AOath
import com.example.twitter.activities.data.repository.RepositoryInt

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TwitterProfileViewModelFactory @Inject constructor(private val repository: RepositoryInt): ViewModelProvider.Factory {

    override fun <T: ViewModel> create(modelClass : Class<T>): T {
        if (modelClass.isAssignableFrom(TwitterProfileViewModel::class.java)) {
            return TwitterProfileViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}