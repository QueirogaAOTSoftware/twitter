package com.example.twitter.activities.connections.apikey

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.example.twitter.activities.connections.AccessTokenData
import javax.inject.Inject

class KeytoApplication  @Inject constructor(context: Application) : AccessTokenData {



    private val mPrefs: SharedPreferences = context.getSharedPreferences("App_prefs", Context.MODE_PRIVATE)

    companion object {
        private const val PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN"

    }


    override var accessToken: String?
        get() = mPrefs.getString(PREF_KEY_ACCESS_TOKEN, null)
        set(accessToken) = mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply()

}