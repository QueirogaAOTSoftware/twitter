package com.example.twitter.activities


import com.example.twitter.activities.searchengine.SearchModule
import com.example.twitter.activities.searchengine.TimelineModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(SearchModule::class)])
    abstract fun TwitterSearcherActivityInjector(): MainActivity
    @ContributesAndroidInjector(modules = [(TimelineModule::class)])
    abstract fun TwitterTimeLineInjector(): ProfileActivity
}