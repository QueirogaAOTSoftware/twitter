package com.example.twitter.activities

import com.google.gson.annotations.SerializedName

data class TweetListData (
    @SerializedName("statuses")
    var tweets: List<Tweet>? = null

)
