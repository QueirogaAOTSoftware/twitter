package com.example.twitter.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.twitter.R
import com.example.twitter.activities.activities.BaseActivity
import com.example.twitter.activities.viewmodel.TwitterMainActivityViewModel
import javax.inject.Inject
import android.widget.SearchView;
import java.io.Serializable


class MainActivity : BaseActivity<TwitterMainActivityViewModel>(), TweetListAdapter.OnItemClickListener {

    @Inject
    lateinit var MainActivityViewModel: TwitterMainActivityViewModel


    private var tweetListAdapter: TweetListAdapter? = null

    override fun getViewModel(): TwitterMainActivityViewModel = MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpRecyclerView()
        setUpSearchView()
        bindViewModels()
    }



    private fun setUpRecyclerView(){

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val myList = findViewById<View>(R.id.rv_tweet) as RecyclerView
        myList.layoutManager = layoutManager
        tweetListAdapter = TweetListAdapter(this,this)
        myList.adapter = tweetListAdapter

    }

    fun searchTweets(q: String){
        var progressBar:ProgressBar = findViewById<View>(R.id.spinner) as ProgressBar
        progressBar.visibility= View.VISIBLE
        MainActivityViewModel.search(q)
    }


    fun bindViewModels(){
        MainActivityViewModel.searchTweetObservable().observe(this, Observer {


            setData(it)
        })
    }

    fun setData(it: List<Tweet>?) {
        var progressBar:ProgressBar = findViewById<View>(R.id.spinner) as ProgressBar
        progressBar.visibility=View.GONE

        it?.let { it1 -> tweetListAdapter?.tweets = it1 }
    }

    private fun setUpSearchView(){

        var sv:SearchView = findViewById<View>(R.id.searchView) as SearchView
        sv.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    unsubscribeTweet()
                    searchTweets(it)
                }
                return true
            }

            override fun onQueryTextChange(p0: String?) = false
        }
        )
    }

    override fun onBackPressed() {
        super.onBackPressed()

        unsubscribeTweet()
    }

    fun unsubscribeTweet(){
        MainActivityViewModel.unSubscribeTweet()
    }

    override fun onItemClick(position: Int) {
        val tweetclicked = tweetListAdapter?.gettweet(position)

        val intent = Intent(this, ProfileActivity::class.java)

        intent.putExtra("userid", tweetclicked?.user?.screenName.toString())

        startActivity(intent)

    }


}

