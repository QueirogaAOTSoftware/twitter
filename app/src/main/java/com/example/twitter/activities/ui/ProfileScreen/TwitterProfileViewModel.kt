package com.example.twitter.activities.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.twitter.activities.Tweet
import com.example.twitter.activities.TwitterProfileData
import com.example.twitter.activities.activities.AOath
import com.example.twitter.activities.data.repository.RepositoryInt
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class TwitterProfileViewModel (val repository: RepositoryInt): BaseViewModel()  {

    var searchUserLiveData = MutableLiveData<TwitterProfileData>()

    fun searchUserObservable() = searchUserLiveData

    var userTweetLiveData = MutableLiveData<List<Tweet>>()
    fun searchTweetObservable() = userTweetLiveData

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    private fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }
    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun clearDispose(){
        compositeDisposable?.let {
            if (!it.isDisposed)
                it.dispose()
        }

    }
    fun initCompositeDisposable(){
        compositeDisposable = CompositeDisposable()
    }

    fun searchUser(screen_name: String){
        addDisposable(  getUserProfile(screen_name).subscribe({
            searchUserLiveData.postValue(it) },
                { Log.e("usererror_throw", "${it}") }))
        addDisposable(  getProfiletweets(screen_name).subscribe({
            userTweetLiveData.postValue(it) },
                { Log.e("timelineerror_throw","${it}") }))
    }

    private fun getUserProfile(screen_name: String) : Observable<TwitterProfileData>{
        return repository.searchprofile(screen_name).observeOn(Schedulers.io()).subscribeOn(Schedulers.io())
    }

    private fun getProfiletweets(screen_name: String) : Observable<List<Tweet>>{
        return repository.searchtimeline(screen_name).observeOn(Schedulers.io()).subscribeOn(Schedulers.io())
    }

    fun unSubscribeUser(){
        clearDispose()
        initCompositeDisposable()
    }
}
