package com.example.twitter.activities.searchengine

import android.app.Activity
import android.app.Application
import com.example.twitter.activities.ApplicationComponent
import com.example.twitter.activities.ApplicationModule
import com.example.twitter.activities.DaggerApplicationComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.reactivex.plugins.RxJavaPlugins
import javax.inject.Inject


class SearchApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    private lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()
        applicationComponent.inject(this)
        

        setupDefaultRxJavaErrorHandler()

    }

    private fun setupDefaultRxJavaErrorHandler() {
        RxJavaPlugins.setErrorHandler { throwable ->
            throwable.printStackTrace()
        }
    }


    override fun activityInjector() = activityInjector



}