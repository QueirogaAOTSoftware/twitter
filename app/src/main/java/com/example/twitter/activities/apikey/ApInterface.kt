package com.example.twitter.activities.connections.apikey


import com.example.twitter.activities.Tweet
import com.example.twitter.activities.TweetListData
import com.example.twitter.activities.TwitterProfileData
import com.example.twitter.activities.connections.AccessToken
import io.reactivex.Observable
import retrofit2.http.*


interface ApInterface {


    companion object {
        const val HEADER_PARAM_SEPARATOR = ":"


    }

    @POST("oauth2/token")
    @FormUrlEncoded
    fun getAccessToken(@Header("Authorization") authorization: String, @Field("grant_type") grantType: String ): Observable<AccessToken>

    @GET("1.1/search/tweets.json")
    fun getTweetList(@Header("Authorization") authorization :String   ,@Query("q")  query : String): Observable<TweetListData>

    @GET("1.1/users/show.json")
    fun getProfileDetails(@Header("Authorization") authorization: String  ,@Query("screen_name") screen_name: String): Observable<TwitterProfileData>

    @GET("1.1/statuses/user_timeline.json")
    fun getTimeLineTweetList(@Header("Authorization") authorization: String  ,@Query("screen_name") screen_name: String): Observable<List<Tweet>>

}