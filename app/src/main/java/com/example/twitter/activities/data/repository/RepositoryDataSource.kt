package com.example.twitter.activities.data.repository


import com.example.twitter.activities.Tweet
import com.example.twitter.activities.TweetListData
import com.example.twitter.activities.TwitterProfileData
import com.example.twitter.activities.activities.AOath
import io.reactivex.Observable
import javax.inject.Inject

class RepositoryDataSource @Inject constructor(private val repoRemoteService: AOath): RepositoryDataSourceInt{

    override fun search(query: String): Observable<TweetListData> {
        return repoRemoteService.searchTweets(query)
    }
    override fun searchtimeline(query: String): Observable<List<Tweet>> {
        return repoRemoteService.searchTimeline(query)
    }
    override fun searchprofile(query: String): Observable<TwitterProfileData> {
        return repoRemoteService.searchProfile(query)
    }


}