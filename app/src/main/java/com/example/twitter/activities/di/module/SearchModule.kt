package com.example.twitter.activities.searchengine

import androidx.lifecycle.ViewModelProvider
import com.example.twitter.activities.MainActivity
import com.example.twitter.activities.viewmodel.TwitterMainActivityViewModel
import com.example.twitter.activities.viewmodel.TwitterMainActivityViewModelFactory
import dagger.Module
import dagger.Provides




@Module
class SearchModule {
    @Provides
    fun provideViewModel(twitterSearcher: MainActivity, ViewModelFactory: TwitterMainActivityViewModelFactory) = ViewModelProvider(twitterSearcher, ViewModelFactory).get(
        TwitterMainActivityViewModel::class.java)

}