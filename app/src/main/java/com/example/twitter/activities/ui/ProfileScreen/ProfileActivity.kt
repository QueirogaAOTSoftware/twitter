package com.example.twitter.activities

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.twitter.R
import com.example.twitter.activities.activities.BaseActivity
import com.example.twitter.activities.viewmodel.TwitterProfileViewModel
import javax.inject.Inject

class ProfileActivity : BaseActivity<TwitterProfileViewModel>()  {
    @Inject   lateinit var  twitterProfileViewModel: TwitterProfileViewModel
    override fun getViewModel(): TwitterProfileViewModel = twitterProfileViewModel

    private var tweetAdapter: TimelineAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_layout)
        val user = intent.getStringExtra("userid").toString()
        val username = findViewById<TextView>(R.id.username)


        username.text = user
        twitterProfileViewModel.searchUser(user)
        twitterProfileViewModel.searchUserObservable().observe(this, Observer {
            setData(it)
        })
        twitterProfileViewModel.searchTweetObservable().observe(this, Observer {
            setTweets(it)
        })

        setUpRecyclerView()
    }

    private fun setUpRecyclerView(){
        var linearLayoutManager= LinearLayoutManager(this)
        val usertweets = findViewById<RecyclerView>(R.id.usertweets)

        usertweets.layoutManager = linearLayoutManager
        tweetAdapter = TimelineAdapter(this)
        usertweets.adapter = tweetAdapter
        usertweets.setNestedScrollingEnabled(false);
    }


    fun setTweets(it : List<Tweet>?){
        it?.let { it1 ->  tweetAdapter?.tweetsList = it1 }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        unsubscribeUser()
    }


    private fun setData(it: TwitterProfileData){
        val name :TextView = findViewById(R.id.name)
        val following :TextView = findViewById(R.id.followingnumber)
        val followers :TextView = findViewById(R.id.followersnumber)
        val profileImg : ImageView = findViewById(R.id.userImage)
        val banner :ImageView = findViewById(R.id.banner)
        val description :TextView = findViewById(R.id.description)
        val location :TextView = findViewById(R.id.location)
        name.text = it.userName
        description.text = it.description
        location.text = it.location
        following.text = it.following.toString()
        followers.text = it.followers.toString()
        val biggerImgUrl = it.imageUrl
        val biggerBannerUrl = it.banner
        Glide.with(this)
                .load(biggerImgUrl)
                .centerCrop()
                .into(profileImg)
        Glide.with(this)
                .load(biggerBannerUrl)
                .centerCrop()
                .into(banner)
    }

    fun unsubscribeUser(){
        twitterProfileViewModel.unSubscribeUser()
    }
}
