package com.example.twitter.activities.data.repository

import com.example.twitter.activities.Tweet
import com.example.twitter.activities.TweetListData
import com.example.twitter.activities.TwitterProfileData
import io.reactivex.Observable
import javax.inject.Inject

class RepositoryImp @Inject constructor(private val repoRemoteDataSource: RepositoryDataSourceInt) : RepositoryInt {


    override fun search(query: String): Observable<TweetListData> {

        val result = repoRemoteDataSource.search(query)



        return result
    }

    override fun searchtimeline(query: String): Observable<List<Tweet>> {
        val result = repoRemoteDataSource.searchtimeline(query)


        return result
    }

    override fun searchprofile(query: String): Observable<TwitterProfileData> {
        val result = repoRemoteDataSource.searchprofile(query)


        return result
    }
}