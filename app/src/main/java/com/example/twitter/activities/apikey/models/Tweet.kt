package com.example.twitter.activities

import com.google.gson.annotations.SerializedName

data class Tweet (
    @SerializedName("user")
    var user: User? = null,

    @SerializedName("text")
    var body: String? = null
)