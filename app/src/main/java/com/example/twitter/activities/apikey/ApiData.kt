package com.example.twitter.activities.connections.apikey


import com.example.twitter.activities.Tweet
import com.example.twitter.activities.TweetListData
import com.example.twitter.activities.TwitterProfileData
import com.example.twitter.activities.connections.AccessToken
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiData @Inject
constructor( private val apiData: ApInterface ) : ApInterfaceData {

   override fun getAccessToken(authorization: String, grantType: String): Observable<AccessToken> = apiData.getAccessToken(authorization,grantType)

   override fun getTweetList(authorization: String, query: String): Observable<TweetListData> = apiData.getTweetList(authorization,query)

   override fun getTimeLineTweetList(authorization: String, screen_name: String): Observable<List<Tweet>> = apiData.getTimeLineTweetList(authorization,screen_name)

   override fun getProfileDetails(authorization: String, screen_name: String): Observable<TwitterProfileData> = apiData.getProfileDetails(authorization,screen_name)

}