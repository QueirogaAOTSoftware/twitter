package com.example.twitter.activities.viewmodel

import androidx.lifecycle.MutableLiveData
import com.example.twitter.activities.Tweet
import com.example.twitter.activities.TweetListData
import com.example.twitter.activities.data.repository.RepositoryInt
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class TwitterMainActivityViewModel(val repository : RepositoryInt) : BaseViewModel()  {

    var searchTweetListData = MutableLiveData<List<Tweet>>()
    fun searchTweetObservable() = searchTweetListData

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    private fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun clearDispose(){

        compositeDisposable?.let {
            if (!it.isDisposed)
                it.dispose()
        }
    }

    fun initCompositeDisposable(){
        compositeDisposable = CompositeDisposable(  )
    }


    fun search(query:String){


        addDisposable(  tweetObservable(query) !!.subscribe{
            searchTweetListData.postValue(it.tweets)
        }
        )
    }


    var tweetObservable : Observable<TweetListData>? = null
    private fun tweetObservable(query: String) : Observable<TweetListData>?{

        tweetObservable=  Observable.interval(0,10, TimeUnit.SECONDS, Schedulers.io())
            .flatMap{
                searchview(query)
            }.observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())

        return tweetObservable

    }

    fun searchview(query: String): Observable<TweetListData> {
        return repository.search(query)
    }

    fun unSubscribeTweet(){
        clearDispose()
        initCompositeDisposable()

    }



}