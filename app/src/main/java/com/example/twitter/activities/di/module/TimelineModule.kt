package com.example.twitter.activities.searchengine

import androidx.lifecycle.ViewModelProvider
import com.example.twitter.activities.ProfileActivity
import com.example.twitter.activities.viewmodel.TwitterProfileViewModel
import com.example.twitter.activities.viewmodel.TwitterProfileViewModelFactory
import dagger.Module
import dagger.Provides



@Module
class TimelineModule {
    @Provides

    fun provideViewModel(twitterSearcher: ProfileActivity, ViewModelFactory: TwitterProfileViewModelFactory) = ViewModelProvider(twitterSearcher, ViewModelFactory).get(
            TwitterProfileViewModel::class.java)
}