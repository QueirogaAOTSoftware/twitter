package com.example.twitter.activities

import com.google.gson.annotations.SerializedName

data class User (

    @SerializedName("name")
    var userName: String? = null,
    @SerializedName("screen_name")
    var screenName: String? = null,
    @SerializedName("profile_image_url_https")
    var profilepic: String? = null


)
