package com.example.twitter.activities.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.twitter.activities.viewmodel.BaseViewModel
import dagger.android.AndroidInjection



abstract class BaseActivity<out VM : BaseViewModel> : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (hasInjector())
            AndroidInjection.inject(this)
    }

    open fun hasInjector() = true

    abstract fun getViewModel(): VM

}