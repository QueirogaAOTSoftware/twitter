package com.example.twitter.activities.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.twitter.activities.activities.AOath
import com.example.twitter.activities.data.repository.RepositoryInt
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class  TwitterMainActivityViewModelFactory @Inject constructor(
    private val repository: RepositoryInt
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TwitterMainActivityViewModel::class.java)) {

            return TwitterMainActivityViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}