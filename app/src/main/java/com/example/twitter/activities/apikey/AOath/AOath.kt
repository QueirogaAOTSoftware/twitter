package com.example.twitter.activities.activities

import android.text.TextUtils
import android.util.Base64
import com.example.twitter.activities.Tweet
import com.example.twitter.activities.TweetListData
import com.example.twitter.activities.TwitterProfileData
import com.example.twitter.activities.connections.AccessToken
import com.example.twitter.activities.connections.AccessTokenData
import com.example.twitter.activities.connections.apikey.ApInterface
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.io.UnsupportedEncodingException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AOath @Inject constructor (private val ApInterface: ApInterface,
                                 private val keyInterface: AccessTokenData) {

    val mConsumerKey: String = "EVOthsDOovZgo8J5FD2WGJjS7"
    val mConsumerSecret: String = "cSPe9sabjRCrgMsNXgUqjW2bHtnr3buUUGJyrTodl5RchAeWN6"

    private val authorizationHeader: String?
        get() {
            try {
                val appKeySecret = "$mConsumerKey:$mConsumerSecret"
                val data = appKeySecret.toByteArray(charset("UTF-8"))
                val base64 = Base64.encodeToString(data, Base64.NO_WRAP)
                return "Basic $base64"
            } catch (e: UnsupportedEncodingException) {
                return null
            }
        }

    private val accessToken: String?
        get() {
            val accessToken = keyInterface.accessToken
            if (TextUtils.isEmpty(accessToken)) {
                return null
            }
            return "Bearer " + accessToken!!
        }
    fun getTweetList(query: String) : Observable<TweetListData> {
        return  ApInterface.getTweetList(accessToken!!, query).observeOn(Schedulers.io()).observeOn(Schedulers.io())
    }

    fun getUserProfile(screen_name: String): Observable<TwitterProfileData>{
        return ApInterface.getProfileDetails(accessToken!!, screen_name).observeOn(Schedulers.io())
    }


    fun getUserTweets(screen_name: String): Observable<List<Tweet>>{
        return ApInterface.getTimeLineTweetList(accessToken!!, screen_name).observeOn(Schedulers.io())
    }



    fun searchTweets(query: String)  : Observable<TweetListData> {
        val accessToken = accessToken
        return if (TextUtils.isEmpty(accessToken)) {
            requestAccessTokenAndGetTweetList(query)
        } else {
            getTweetList(query)
        }
    }

    fun searchProfile(screen_name: String)  : Observable<TwitterProfileData> {
        val accessToken = accessToken
        return if (TextUtils.isEmpty(accessToken)) {
            requestAccessTokenAndGetProfile(screen_name)
        } else {
            getUserProfile(screen_name)
        }
    }

    fun searchTimeline(query: String)  : Observable<List<Tweet>> {
        val accessToken = accessToken
        return if (TextUtils.isEmpty(accessToken)) {
            requestAccessTokenAndGetTimeline(query)
        } else {
            getUserTweets(query)
        }
    }



    private fun saveAccessToken(accessToken: String) {
        keyInterface.accessToken =accessToken
    }

    private fun requestAccessTokenAndGetTweetList(
            query: String
    )  : Observable<TweetListData> {

        return   reqAccessToken()
                .flatMap {
                    getTweetList(query)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
    }

    private fun requestAccessTokenAndGetProfile(
            screen_name: String
    )  : Observable<TwitterProfileData> {

        return   reqAccessToken()
                .flatMap {
                    getUserProfile(screen_name)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
    }

    private fun requestAccessTokenAndGetTimeline(
            query: String
    )  : Observable<List<Tweet>> {

        return   reqAccessToken()
                .flatMap {
                    getUserTweets(query)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
    }

    fun reqAccessToken() : Observable<AccessToken> {

        return ApInterface.getAccessToken(authorizationHeader!!, "client_credentials")
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .doOnNext {
                    saveAccessToken(it.accessToken)
                }


    }
}
