package com.example.twitter.activities

import android.app.Application
import android.content.Context
import com.example.twitter.activities.connections.AccessTokenData
import com.example.twitter.activities.connections.ConnectionModule
import com.example.twitter.activities.connections.apikey.KeytoApplication
import com.example.twitter.activities.data.repository.RepositoryDataSource
import com.example.twitter.activities.data.repository.RepositoryDataSourceInt
import com.example.twitter.activities.data.repository.RepositoryImp
import com.example.twitter.activities.data.repository.RepositoryInt
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [(ConnectionModule::class)])
class ApplicationModule (val app: Application){

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideContext(): Context =app


    @Provides
    @Singleton
    internal fun providePreferencesHelper(keytoApplication: KeytoApplication): AccessTokenData = keytoApplication

    @Provides
    @Singleton
    internal fun provideRepositoryRoute(repository: RepositoryImp): RepositoryInt = repository

    @Provides
    @Singleton
    internal fun provideRepositoryData(repository: RepositoryDataSource): RepositoryDataSourceInt = repository

}